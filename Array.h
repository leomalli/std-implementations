#ifndef _ARRAY_H_
#define _ARRAY_H_

#include <cuchar>
#include <iterator>

#define NOTDONE() \
    do { \
        fprintf(stderr, "%s:%d: TODO: %s() is not implemented yet\n",  __FILE__, __LINE__, __func__); \
        abort(); \
    } while(0)



namespace leo {

template <typename myArr>
class ArrayIterator
{
public:
    using iterator_category = std::forward_iterator_tag;
    using difference_type   = std::ptrdiff_t;
    using value_type        = typename myArr::value_type;
    using pointer           = value_type*;
    using reference         = value_type&;

public:
    ArrayIterator(pointer ptr): m_Ptr(ptr) {}
    ~ArrayIterator() {}

    ArrayIterator& operator++()
    { m_Ptr++; return *this; }

    ArrayIterator operator++(int)
    { ArrayIterator tmp = *this; ++(*this); return tmp; }

    ArrayIterator& operator--()
    { m_Ptr--; return *this; }

    ArrayIterator operator--(int)
    { ArrayIterator tmp = *this; --(*this); return tmp; }

    reference operator[](size_t idx)
    { return *(m_Ptr + idx);}
    pointer operator->()
    { return m_Ptr;}
    reference operator*()
    { return *m_Ptr;}



    bool operator==(const ArrayIterator& other) const
    {return m_Ptr == other.m_Ptr;}
    bool operator!=(const ArrayIterator& other) const
    {return !(*this == other);}


private:
    pointer m_Ptr;

};



template <typename Tp_, size_t Nm_>
class Array
{
public:
    using value_type        =       Tp_;
    using pointer           =       value_type*;
    using const_pointer     = const value_type*;
    using reference         =       value_type&;
    using const_reference   = const value_type&;

    using iterator          =       ArrayIterator<Array<value_type, Nm_>>;
    using reverse_iterator  =       std::reverse_iterator<iterator>;

private:
    value_type mp_Data[Nm_];
    const size_t m_Size{Nm_};

public:
    Array() = default;
    ~Array() = default;
    template <class... value_type>
    Array(value_type... args)
    {
        static_assert(sizeof...(args) <= Nm_);
        for (size_t i{0zu}; auto& x : {args...})
            mp_Data[i++] = x;
    }

    reference operator[](const size_t& idx)
    { return mp_Data[idx]; }
    reference at(const size_t idx)
    {
        if (idx >= m_Size) throw std::out_of_range();
        return mp_Data[idx];
    }

    size_t size() const
    { return m_Size; }

    iterator begin()
    { return iterator(mp_Data); }
    iterator end()
    { return iterator(mp_Data + m_Size); }
    reverse_iterator rbegin()
    { return reverse_iterator(end()); }
    reverse_iterator rend()
    { return reverse_iterator(begin()); }

    reference front()
    { return mp_Data[0]; }
    reference back()
    { return mp_Data[m_Size - 1]; }

    pointer data()
    { return mp_Data; }
};

template <typename Tp_, size_t Nm_>
bool operator==(const Array<Tp_, Nm_>& lhs, const Array<Tp_, Nm_>& rhs)
{ return std::equal(lhs.begin(), lhs.end(), rhs.begin()); }
template <typename Tp_, size_t Nm_>
bool operator!=(const Array<Tp_, Nm_>& lhs, const Array<Tp_, Nm_>& rhs)
{ return !(lhs == rhs); }


}
#endif