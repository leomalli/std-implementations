#ifndef _LINKED_LIST_H_
#define _LINKED_LIST_H_

#include <cuchar>

#define NOTDONE() \
    do { \
        fprintf(stderr, "%s:%d: TODO: %s() is not implemented yet\n",  __FILE__, __LINE__, __func__); \
        abort(); \
    } while(0)

namespace leo
{

template <typename myList>
class ListIterator
{
public:
    using iterator_category = std::forward_iterator_tag;
    using difference_type   = std::ptrdiff_t;
    using value_type        = typename myList::value_type;
    using pointer           = value_type*;
    using reference         = value_type&;

    using Node              = typename myList::Node;
    using node_pointer      = Node*;

public:
    ListIterator(node_pointer ptr, node_pointer actual_last = nullptr)
    : m_NodePtr(ptr), m_LastElem(actual_last) {}
    ~ListIterator() {}

    ListIterator& operator++()
    { m_NodePtr = m_NodePtr->next; return *this; }

    ListIterator operator++(int)
    { ListIterator tmp = *this; ++(*this); return tmp; }

    ListIterator& operator--()
    { 
        if (m_NodePtr == nullptr)
        {
            m_NodePtr = m_LastElem;
            return *this;
        }
        m_NodePtr = m_NodePtr->previous;
        return *this;
    }

    ListIterator operator--(int)
    { ListIterator tmp = *this; --(*this); return tmp; }

    reference operator[](size_t idx)
    {
        node_pointer tmp = m_NodePtr;
        for (size_t i = 0; i < idx; ++i)
            tmp = m_NodePtr->next;
        
        return tmp->element;
    }
    pointer operator->()
    { return m_NodePtr;}
    reference operator*()
    { return m_NodePtr->value;}



    bool operator==(const ListIterator& other) const
    {return m_NodePtr == other.m_NodePtr;}
    bool operator!=(const ListIterator& other) const
    {return !(*this == other);}


private:
    node_pointer m_NodePtr, m_LastElem;

};

template <typename T_>
class LinkedList
{
public:
    using value_type        = T_;
    using pointer           = value_type*;
    using const_pointer     = const value_type*;
    using reference         = value_type&;
    using const_reference   = const value_type&;

    struct Node
    {
        using node_pointer    = Node*;
        node_pointer previous = nullptr;
        node_pointer next     = nullptr;
        value_type value      = value_type();
    };

    using node_pointer      = Node*;
    using iterator          = ListIterator<LinkedList<value_type>>;
    using reverse_iterator  = std::reverse_iterator<iterator>;
    
private:
    node_pointer m_Beginning{nullptr}, m_Last{nullptr};
    size_t m_Size{0};


public:
    LinkedList() = default;
    ~LinkedList()
    {
        node_pointer next, tmp = m_Beginning;
        for (size_t i = 0; i < m_Size; ++i)
        {
            next = tmp->next;
            delete tmp;
            tmp = next;
        }

    }
    template <typename... value_type>
    LinkedList(value_type... args)
    {
        for (auto& val : {args...})
            this->append(val);
    }

    size_t size() const
    { return m_Size; }

    iterator begin() const
    { return iterator(m_Beginning); }
    iterator end() const
    { return iterator(nullptr, m_Last); }
    reverse_iterator rbegin() const
    { return reverse_iterator( end() );}
    reverse_iterator rend() const
    { return reverse_iterator( begin() );}


    void append( const_reference val )
    {
        node_pointer element = new Node;
        element->value = val;
        // If it is the first element we add
        if (m_Beginning == nullptr)
        {
            m_Beginning = element;
            m_Last = element;
        }
        else
        {
            m_Last->next = element;
            element->previous = m_Last;
            m_Last = element;
        }
        ++m_Size;
    }


    reference operator[](size_t idx)
    {
        node_pointer tmp = m_Beginning;
        while( idx-- > 0)
        {
            tmp = tmp->next;
        }
        return tmp->value;
    }

};

}
#endif