CC := g++

CXXFLAGS := -g -Wall -Wshadow -Wextra -pedantic -std=c++20 -fsanitize=address

# Profile flags: -pg


all: exec

exec: main.o
	$(CC) $(CXXFLAGS) $(PROFILINGFLAGS) -o $@ $<

main.o: main.cpp Vector.h
	$(CC) $(CXXFLAGS) $(PROFILINGFLAGS) -c -o $@ $<

%.h:

profiler: PROFILINGFLAGS := -pg
profiler: all


clean:
	@rm -fv *.o exec *.out *.log
