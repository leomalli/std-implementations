#ifndef _VECTOR_H_
#define _VECTOR_H_

#include <algorithm>
#include <iterator>
#include <utility>

#define NOTDONE() \
    do { \
        fprintf(stderr, "%s:%d: TODO: %s() is not implemented yet\n",  __FILE__, __LINE__, __func__); \
        abort(); \
    } while(0)




namespace leo
{

// Implementation of the iterator class
template <class myVec>
class VectorIterator
{
public:
    using iterator_category = std::forward_iterator_tag;
    using difference_type = std::ptrdiff_t;
    using value_type = typename myVec::value_type;
    using pointer = value_type*;
    using reference = value_type&;

public:
    VectorIterator(pointer ptr)
    : m_Ptr(ptr)
    {}
    ~VectorIterator()
    {}

    VectorIterator& operator++()
    {
        m_Ptr++;
        return *this;
    }
    VectorIterator operator++(int)
    {
        VectorIterator tmp = *this;
        ++(*this);
        return tmp;
    }

    VectorIterator& operator--()
    {
        m_Ptr--;
        return *this;
    }
    VectorIterator operator--(int)
    {
        VectorIterator tmp = *this;
        --(*this);
        return tmp;
    }

    reference operator[](size_t idx)
    { return *(m_Ptr + idx); }

    pointer operator->()
    { return m_Ptr; }
    reference operator*()
    { return *m_Ptr; }

    bool operator==(const VectorIterator& other) const
    { return m_Ptr == other.m_Ptr; }

    bool operator!=(const VectorIterator& other) const
    { return !(*this == other); }

private:
    pointer m_Ptr;

};

// Implementation of the vector class

template <class T = int>
class Vector
{
public:
    using value_type = T;
    using iterator = VectorIterator<Vector<T>>;
    using reverse_iterator = std::reverse_iterator<iterator>;

private:
    size_t m_Size{0}, m_LastElem{0};
    T* mp_Data{nullptr};

    void m_rangeCheck(const size_t& idx) const;
    void m_Relocate( T* newData, const size_t& oldSize );

public:
    Vector() = default;
    Vector(const size_t& size, const T& value = value_type() );
    template <typename... value_type>
    Vector(value_type... args)
    {
        this->reserve(sizeof...(value_type));
        for (auto& x : {args...})
            this->push_back(x);
    }
    ~Vector();

    size_t size() const;
    iterator begin() const;
    iterator end() const;
    reverse_iterator rbegin() const;
    reverse_iterator rend() const;

    void push_back( const T& value );
    void push_back( T&& value );
    void emplace_back( T&& value );
    void emplace_back( const T& value );
    void reserve( const size_t& size );

    T& operator[](const size_t& idx);
    const T& operator[](const size_t& idx) const;

};


template <class T> 
Vector<T>::Vector(const size_t& size, const T& value)
: m_Size(size), m_LastElem(size), mp_Data(nullptr)
{ 
    this->reserve(m_Size);
    std::fill_n(mp_Data, m_Size, value);
}

template <class T>
Vector<T>::~Vector()
{ delete[] mp_Data; }

template <class T>
T& Vector<T>::operator[](const size_t& idx)
{
    return mp_Data[idx];
}

template <class T>
const T& Vector<T>::operator[](const size_t& idx) const
{
    return mp_Data[idx];
}



template <class T>
size_t Vector<T>::size() const
{ return m_LastElem; }

template <class T>
typename Vector<T>::iterator Vector<T>::begin() const
{ return iterator(mp_Data); }

template <class T>
typename Vector<T>::iterator Vector<T>::end() const
{ return iterator(mp_Data + m_Size); }

template <class T>
typename Vector<T>::reverse_iterator Vector<T>::rbegin() const
{ return reverse_iterator( end() ); }

template <class T>
typename Vector<T>::reverse_iterator Vector<T>::rend() const
{ return reverse_iterator( begin() ); }

template <class T>
void Vector<T>::push_back( const T& value )
{
    // If we have enough space we don't need to relocate data
    if (m_Size > m_LastElem)
    {
        mp_Data[m_LastElem++] = value;
        return;
    }

    // Otherwise find new space somewhere on the heap
    this->reserve(m_Size + 1);
    mp_Data[m_LastElem++] = value;
    return;
}

template <class T>
void Vector<T>::push_back( T&& value )
{ this->emplace_back( value ); }

template <class T>
void Vector<T>::emplace_back( T&& value )
{
    // If we have space we don't need to relocate data
    if (m_Size > m_LastElem)
    {
        mp_Data[m_LastElem++] = std::move(value);
        return;
    }

    // Otherwise find new space somewhere on the heap
    this->reserve(m_Size + 1);
    mp_Data[m_LastElem++] = std::move(value);
    return;
}

template <class T>
void Vector<T>::emplace_back( const T& value)
{
    T tmp_val = value;
    this->emplace_back( std::forward<T>(tmp_val) );
}


template <class T>
void Vector<T>::reserve( const size_t& size)
{
    // If we already have sufficient size (and mp_Data points somewhere)
    if (size == m_Size && mp_Data != nullptr) return;

    if (size < m_Size)
        throw("ERROR: Cannot reserve less space than already taken by vector\n");

    T* newData = new T[size];
    // If the object does not hold data yet
    if (mp_Data == nullptr)
    {
        mp_Data = newData;
        m_Size = size;
        return;
    }

    this->m_Relocate(newData, m_Size);
    m_Size = size;
}

template <class T>
void Vector<T>::m_rangeCheck(const size_t& idx) const
{
    if (idx >= m_Size)
        throw std::out_of_range("Trying to access value out of vector's range.");

}


// Takes as input pointer and move data to this destination
template <class T>
void Vector<T>::m_Relocate( T* newData, const size_t& oldSize )
{
    std::copy(mp_Data, mp_Data + oldSize, newData);
    delete[] mp_Data;
    mp_Data = newData;
}


} // namespace leo
#endif