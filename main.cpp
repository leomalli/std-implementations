#include <iostream>
#include <string>
#include <vector>

#include "LinkedList.h"
#include "Vector.h"
#include "Array.h"

int main()
{
using namespace std::string_literals;

    // Vector
    std::cout << "Vector:\n";
    leo::Vector<float> vec = {1.f, -3.23f, 80.f};
    vec[0] = -100.0f; // Random access
    vec.emplace_back(3.141592f);
    // Both reverse and normal iterators
    for (auto& x : vec)
        x = (x < 0.f) ? 0.f : x;
    
    {
        size_t count = 0;
        for (auto it = vec.rbegin(); it != vec.rend(); ++it)
            std::cout << *it << (++count == vec.size() ? "\n" : ", ");
    }

    // Array same as for the vector
    std::cout << "Array:\n";
    leo::Array<size_t, 10> arr = {0,1,2,3,4,5,6,7,8,9};
    --arr[0];
    for (auto i{0zu}; const auto& x : arr)
        std::cout << x << ( ++i == arr.size() ? "\n"s : ", "s);
    
    // Linked list
    std::cout << "Linked List:\n";
    leo::LinkedList<int> list = {1,2,3,4};
    list.append(42);// Append to list
    list[2] = -1;   // Random access
    // Reverse iterator
    {
        size_t count = 0;
        for (auto it = list.rbegin(); it != list.rend(); ++it)
            std::cout << *it << (++count == list.size() ? "\n" : ", ");
    }
    // Normal iterator enables ranged based for loop
    for (auto i{0zu}; const auto& x : list)
        std::cout << x << ( ++i == list.size() ? "\n"s : ", "s);
}