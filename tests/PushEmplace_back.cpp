#include <vector>

#include <assert.h>
#include "../Vector.h"

#define PASSED() fprintf(stdout, "%s: PASSED\t%s\n",  __FILE__, __func__)

#define LENGTH 10'000

// Testing push_back
void test_int_push_back_without_alloc();
void test_float_push_back_without_alloc();
void test_double_push_back_without_alloc();

void test_int_push_back_with_alloc();
void test_float_push_back_with_alloc();
void test_double_push_back_with_alloc();

// Testing emplace_back
void test_int_emplace_back_without_alloc();
void test_float_emplace_back_without_alloc();
void test_double_emplace_back_without_alloc();

void test_int_emplace_back_with_alloc();
void test_float_emplace_back_with_alloc();
void test_double_emplace_back_with_alloc();


// Same testing as above but with reserve
// Testing push_back
void test_int_push_back_without_alloc_with_reserve();
void test_float_push_back_without_alloc_with_reserve();
void test_double_push_back_without_alloc_with_reserve();

void test_int_push_back_with_alloc_with_reserve();
void test_float_push_back_with_alloc_with_reserve();
void test_double_push_back_with_alloc_with_reserve();

// Testing emplace_back
void test_int_emplace_back_without_alloc_with_reserve();
void test_float_emplace_back_without_alloc_with_reserve();
void test_double_emplace_back_without_alloc_with_reserve();

void test_int_emplace_back_with_alloc_with_reserve();
void test_float_emplace_back_with_alloc_with_reserve();
void test_double_emplace_back_with_alloc_with_reserve();


int main()
{

    test_int_push_back_without_alloc();
    test_float_push_back_without_alloc();
    test_double_push_back_without_alloc();

    test_int_push_back_with_alloc();
    test_float_push_back_with_alloc();
    test_double_push_back_with_alloc();

    test_int_emplace_back_without_alloc();
    test_float_emplace_back_without_alloc();
    test_double_emplace_back_without_alloc();

    test_int_emplace_back_with_alloc();
    test_float_emplace_back_with_alloc();
    test_double_emplace_back_with_alloc();


    test_int_push_back_without_alloc_with_reserve();
    test_float_push_back_without_alloc_with_reserve();
    test_double_push_back_without_alloc_with_reserve();
    
    test_int_push_back_with_alloc_with_reserve();
    test_float_push_back_with_alloc_with_reserve();
    test_double_push_back_with_alloc_with_reserve();
    
    test_int_emplace_back_without_alloc_with_reserve();
    test_float_emplace_back_without_alloc_with_reserve();
    test_double_emplace_back_without_alloc_with_reserve();
    
    test_int_emplace_back_with_alloc_with_reserve();
    test_float_emplace_back_with_alloc_with_reserve();
    test_double_emplace_back_with_alloc_with_reserve();



    return EXIT_SUCCESS;
}



// TESTING PUSH_BACK
void test_int_push_back_without_alloc()
{
    std::vector<int> stdVec;
    leo::Vector<int> myVec;

    for (int i = 0; i < LENGTH; ++i)
    {
        stdVec.push_back(i);
        myVec.push_back(i);
    }

    assert(stdVec.size() == myVec.size());
    for (int i = 0; i < LENGTH; ++i)
        assert (stdVec[i] == myVec[i]);

    PASSED();
}

void test_float_push_back_without_alloc()
{
    std::vector<float> stdVec;
    leo::Vector<float> myVec;

    for (int i = 0; i < LENGTH; ++i)
    {
        stdVec.push_back(i);
        myVec.push_back(i);
    }

    assert(stdVec.size() == myVec.size());
    for (int i = 0; i < LENGTH; ++i)
        assert (stdVec[i] == myVec[i]);

    PASSED();
}
void test_double_push_back_without_alloc()
{
    std::vector<double> stdVec;
    leo::Vector<double> myVec;

    for (int i = 0; i < LENGTH; ++i)
    {
        stdVec.push_back(i);
        myVec.push_back(i);
    }

    assert(stdVec.size() == myVec.size());
    for (int i = 0; i < LENGTH; ++i)
        assert (stdVec[i] == myVec[i]);

    PASSED();
}

void test_int_push_back_with_alloc()
{
    std::vector<int> stdVec(LENGTH);
    leo::Vector<int> myVec(LENGTH);

    for (int i = 0; i < LENGTH; ++i)
    {
        stdVec.push_back(i);
        myVec.push_back(i);
    }

    assert(stdVec.size() == myVec.size());
    for (int i = 0; i < LENGTH; ++i)
        assert (stdVec[i] == myVec[i]);

    PASSED();
}

void test_float_push_back_with_alloc()
{
    std::vector<float> stdVec(LENGTH);
    leo::Vector<float> myVec(LENGTH);

    for (int i = 0; i < LENGTH; ++i)
    {
        stdVec.push_back(i);
        myVec.push_back(i);
    }

    assert(stdVec.size() == myVec.size());
    for (int i = 0; i < LENGTH; ++i)
        assert (stdVec[i] == myVec[i]);

    PASSED();
}
void test_double_push_back_with_alloc()
{
    std::vector<double> stdVec(LENGTH);
    leo::Vector<double> myVec(LENGTH);

    for (int i = 0; i < LENGTH; ++i)
    {
        stdVec.push_back(i);
        myVec.push_back(i);
    }

    assert(stdVec.size() == myVec.size());
    for (int i = 0; i < LENGTH; ++i)
        assert (stdVec[i] == myVec[i]);

    PASSED();
}


// TESTING EMPLACE_BACK
void test_int_emplace_back_without_alloc()
{
    std::vector<int> stdVec;
    leo::Vector<int> myVec;

    for (int i = 0; i < LENGTH; ++i)
    {
        stdVec.emplace_back(i);
        myVec.emplace_back(i);
    }

    assert(stdVec.size() == myVec.size());
    for (int i = 0; i < LENGTH; ++i)
        assert (stdVec[i] == myVec[i]);

    PASSED();
}

void test_float_emplace_back_without_alloc()
{
    std::vector<float> stdVec;
    leo::Vector<float> myVec;

    for (int i = 0; i < LENGTH; ++i)
    {
        stdVec.emplace_back(i);
        myVec.emplace_back(i);
    }

    assert(stdVec.size() == myVec.size());
    for (int i = 0; i < LENGTH; ++i)
        assert (stdVec[i] == myVec[i]);

    PASSED();
}
void test_double_emplace_back_without_alloc()
{
    std::vector<double> stdVec;
    leo::Vector<double> myVec;

    for (int i = 0; i < LENGTH; ++i)
    {
        stdVec.emplace_back(i);
        myVec.emplace_back(i);
    }

    assert(stdVec.size() == myVec.size());
    for (int i = 0; i < LENGTH; ++i)
        assert (stdVec[i] == myVec[i]);

    PASSED();
}

void test_int_emplace_back_with_alloc()
{
    std::vector<int> stdVec(LENGTH);
    leo::Vector<int> myVec(LENGTH);

    for (int i = 0; i < LENGTH; ++i)
    {
        stdVec.emplace_back(i);
        myVec.emplace_back(i);
    }

    assert(stdVec.size() == myVec.size());
    for (int i = 0; i < LENGTH; ++i)
        assert (stdVec[i] == myVec[i]);

    PASSED();
}

void test_float_emplace_back_with_alloc()
{
    std::vector<float> stdVec(LENGTH);
    leo::Vector<float> myVec(LENGTH);

    for (int i = 0; i < LENGTH; ++i)
    {
        stdVec.emplace_back(i);
        myVec.emplace_back(i);
    }

    assert(stdVec.size() == myVec.size());
    for (int i = 0; i < LENGTH; ++i)
        assert (stdVec[i] == myVec[i]);

    PASSED();
}
void test_double_emplace_back_with_alloc()
{
    std::vector<double> stdVec(LENGTH);
    leo::Vector<double> myVec(LENGTH);

    for (int i = 0; i < LENGTH; ++i)
    {
        stdVec.emplace_back(i);
        myVec.emplace_back(i);
    }

    assert(stdVec.size() == myVec.size());
    for (int i = 0; i < LENGTH; ++i)
        assert (stdVec[i] == myVec[i]);

    PASSED();
}

// TESTING EVERYTHING WITH RESERVE

// TESTING PUSH_BACK
void test_int_push_back_without_alloc_with_reserve()
{
    std::vector<int> stdVec;
	stdVec.reserve(LENGTH);
    leo::Vector<int> myVec;
	myVec.reserve(LENGTH);

    for (int i = 0; i < LENGTH; ++i)
    {
        stdVec.push_back(i);
        myVec.push_back(i);
    }

    assert(stdVec.size() == myVec.size());
    for (int i = 0; i < LENGTH; ++i)
        assert (stdVec[i] == myVec[i]);

    PASSED();
}

void test_float_push_back_without_alloc_with_reserve()
{
    std::vector<float> stdVec;
	stdVec.reserve(LENGTH);
    leo::Vector<float> myVec;
	myVec.reserve(LENGTH);

    for (int i = 0; i < LENGTH; ++i)
    {
        stdVec.push_back(i);
        myVec.push_back(i);
    }

    assert(stdVec.size() == myVec.size());
    for (int i = 0; i < LENGTH; ++i)
        assert (stdVec[i] == myVec[i]);

    PASSED();
}
void test_double_push_back_without_alloc_with_reserve()
{
    std::vector<double> stdVec;
	stdVec.reserve(LENGTH);
    leo::Vector<double> myVec;
	myVec.reserve(LENGTH);

    for (int i = 0; i < LENGTH; ++i)
    {
        stdVec.push_back(i);
        myVec.push_back(i);
    }

    assert(stdVec.size() == myVec.size());
    for (int i = 0; i < LENGTH; ++i)
        assert (stdVec[i] == myVec[i]);

    PASSED();
}

void test_int_push_back_with_alloc_with_reserve()
{
    std::vector<int> stdVec(LENGTH);
	stdVec.reserve(LENGTH);
    leo::Vector<int> myVec(LENGTH);
	myVec.reserve(LENGTH);

    for (int i = 0; i < LENGTH; ++i)
    {
        stdVec.push_back(i);
        myVec.push_back(i);
    }

    assert(stdVec.size() == myVec.size());
    for (int i = 0; i < LENGTH; ++i)
        assert (stdVec[i] == myVec[i]);

    PASSED();
}

void test_float_push_back_with_alloc_with_reserve()
{
    std::vector<float> stdVec(LENGTH);
	stdVec.reserve(LENGTH);
    leo::Vector<float> myVec(LENGTH);
	myVec.reserve(LENGTH);

    for (int i = 0; i < LENGTH; ++i)
    {
        stdVec.push_back(i);
        myVec.push_back(i);
    }

    assert(stdVec.size() == myVec.size());
    for (int i = 0; i < LENGTH; ++i)
        assert (stdVec[i] == myVec[i]);

    PASSED();
}
void test_double_push_back_with_alloc_with_reserve()
{
    std::vector<double> stdVec(LENGTH);
	stdVec.reserve(LENGTH);
    leo::Vector<double> myVec(LENGTH);
	myVec.reserve(LENGTH);

    for (int i = 0; i < LENGTH; ++i)
    {
        stdVec.push_back(i);
        myVec.push_back(i);
    }

    assert(stdVec.size() == myVec.size());
    for (int i = 0; i < LENGTH; ++i)
        assert (stdVec[i] == myVec[i]);

    PASSED();
}


// TESTING EMPLACE_BACK
void test_int_emplace_back_without_alloc_with_reserve()
{
    std::vector<int> stdVec;
	stdVec.reserve(LENGTH);
    leo::Vector<int> myVec;
	myVec.reserve(LENGTH);

    for (int i = 0; i < LENGTH; ++i)
    {
        stdVec.emplace_back(i);
        myVec.emplace_back(i);
    }

    assert(stdVec.size() == myVec.size());
    for (int i = 0; i < LENGTH; ++i)
        assert (stdVec[i] == myVec[i]);

    PASSED();
}

void test_float_emplace_back_without_alloc_with_reserve()
{
    std::vector<float> stdVec;
	stdVec.reserve(LENGTH);
    leo::Vector<float> myVec;
	myVec.reserve(LENGTH);

    for (int i = 0; i < LENGTH; ++i)
    {
        stdVec.emplace_back(i);
        myVec.emplace_back(i);
    }

    assert(stdVec.size() == myVec.size());
    for (int i = 0; i < LENGTH; ++i)
        assert (stdVec[i] == myVec[i]);

    PASSED();
}
void test_double_emplace_back_without_alloc_with_reserve()
{
    std::vector<double> stdVec;
	stdVec.reserve(LENGTH);
    leo::Vector<double> myVec;
	myVec.reserve(LENGTH);

    for (int i = 0; i < LENGTH; ++i)
    {
        stdVec.emplace_back(i);
        myVec.emplace_back(i);
    }

    assert(stdVec.size() == myVec.size());
    for (int i = 0; i < LENGTH; ++i)
        assert (stdVec[i] == myVec[i]);

    PASSED();
}

void test_int_emplace_back_with_alloc_with_reserve()
{
    std::vector<int> stdVec(LENGTH);
	stdVec.reserve(LENGTH);
    leo::Vector<int> myVec(LENGTH);
	myVec.reserve(LENGTH);

    for (int i = 0; i < LENGTH; ++i)
    {
        stdVec.emplace_back(i);
        myVec.emplace_back(i);
    }

    assert(stdVec.size() == myVec.size());
    for (int i = 0; i < LENGTH; ++i)
        assert (stdVec[i] == myVec[i]);

    PASSED();
}

void test_float_emplace_back_with_alloc_with_reserve()
{
    std::vector<float> stdVec(LENGTH);
	stdVec.reserve(LENGTH);
    leo::Vector<float> myVec(LENGTH);
	myVec.reserve(LENGTH);

    for (int i = 0; i < LENGTH; ++i)
    {
        stdVec.emplace_back(i);
        myVec.emplace_back(i);
    }

    assert(stdVec.size() == myVec.size());
    for (int i = 0; i < LENGTH; ++i)
        assert (stdVec[i] == myVec[i]);

    PASSED();
}
void test_double_emplace_back_with_alloc_with_reserve()
{
    std::vector<double> stdVec(LENGTH);
	stdVec.reserve(LENGTH);
    leo::Vector<double> myVec(LENGTH);
	myVec.reserve(LENGTH);

    for (int i = 0; i < LENGTH; ++i)
    {
        stdVec.emplace_back(i);
        myVec.emplace_back(i);
    }

    assert(stdVec.size() == myVec.size());
    for (int i = 0; i < LENGTH; ++i)
        assert (stdVec[i] == myVec[i]);

    PASSED();
}