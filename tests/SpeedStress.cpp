#include <chrono>
#include <random>
#include <vector>
#include "../Vector.h"

#define PASSED() fprintf(stdout, "%s: %s PASSED\n",  __FILE__, __func__)

#define NOW() std::chrono::steady_clock::now()
#define sTIME(BEGIN, END) std::chrono::duration_cast<std::chrono::seconds>(END - BEGIN).count()
#define msTIME(BEGIN, END) std::chrono::duration_cast<std::chrono::milliseconds>(END - BEGIN).count()

#define RESULTS(STD, MINE) fprintf(stdout, "%s: STD: %lims, MINE: %lims\t\t%s\n",  __FILE__, STD, MINE, __func__)

#define ACCESS_VEC_SIZE 100'000
#define ACCESS_DURATION 10'000'000
#define CREATE_DURATION 1'000'000
#define CREATE_VEC_SIZE 1'000

#define PUSH_SIZE 100'000

void test_random_access_speed();
void test_random_write_speed();

void test_empty_instanciation_speed();
void test_full_instanciation_speed();
void test_push_speed();
void test_emplace_speed();

int main()
{


    test_random_access_speed();
    test_random_write_speed();
    test_empty_instanciation_speed();
    test_full_instanciation_speed();
    test_push_speed();
    test_emplace_speed();


    return EXIT_SUCCESS;
}



void test_random_access_speed()
{
    std::vector<int> stdVec(ACCESS_VEC_SIZE);
    leo::Vector<int> leoVec(ACCESS_VEC_SIZE);

    std::random_device rd;
    std::mt19937 tw(rd());
    std::uniform_int_distribution<int> dist(0, ACCESS_VEC_SIZE);

    auto begin = NOW();
    for (int i = 0; i < ACCESS_DURATION; ++i)
        stdVec[dist(tw)];

    auto end = NOW();
    auto stdTIME = msTIME(begin, end);

    begin = NOW();
    for (int i = 0; i < ACCESS_DURATION; ++i)
        leoVec[dist(tw)];
    
    end = NOW();
    auto leoTIME = msTIME(begin, end);

    RESULTS(stdTIME, leoTIME);
}

void test_random_write_speed()
{
    std::vector<int> stdVec(ACCESS_VEC_SIZE);
    leo::Vector<int> leoVec(ACCESS_VEC_SIZE);

    std::random_device rd;
    std::mt19937 tw(rd());
    std::uniform_int_distribution<int> acces(0, ACCESS_VEC_SIZE);
    std::uniform_int_distribution<int> dist(INT32_MIN, INT32_MAX);

    auto begin = NOW();
    for (int i = 0; i < ACCESS_DURATION; ++i)
        stdVec[acces(tw)] = dist(tw);

    auto end = NOW();
    auto stdTIME = msTIME(begin, end);

    begin = NOW();
    for (int i = 0; i < ACCESS_DURATION; ++i)
        leoVec[acces(tw)] = dist(tw);
    
    end = NOW();
    auto leoTIME = msTIME(begin, end);

    RESULTS(stdTIME, leoTIME);
}
void test_empty_instanciation_speed()
{
    auto begin = NOW();
    for (int i = 0; i < CREATE_DURATION; ++i)
        std::vector<int> stdVec;

    auto end = NOW();
    auto stdTIME = msTIME(begin, end);

    begin = NOW();
    for (int i = 0; i < CREATE_DURATION; ++i)
        leo::Vector<int> leoVec;

    end = NOW();
    auto leoTIME = msTIME(begin, end);
    RESULTS(stdTIME, leoTIME);
}

void test_full_instanciation_speed()
{
    auto begin = NOW();
    for (int i = 0; i < CREATE_DURATION; ++i)
        std::vector<int> stdVec(CREATE_VEC_SIZE);

    auto end = NOW();
    auto stdTIME = msTIME(begin, end);

    begin = NOW();
    for (int i = 0; i < CREATE_DURATION; ++i)
        leo::Vector<int> leoVec(CREATE_VEC_SIZE);

    end = NOW();
    auto leoTIME = msTIME(begin, end);
    RESULTS(stdTIME, leoTIME);
}

void test_push_speed()
{
    std::vector<int> stdVec;
    leo::Vector<int> leoVec;
    std::random_device rd;
    std::mt19937 tw(rd());
    std::uniform_int_distribution<int> dist(INT32_MIN, INT32_MAX);

    auto begin = NOW();
    for (int i = 0; i < PUSH_SIZE; ++i)
        stdVec.push_back(dist(tw));

    auto end = NOW();
    auto stdTIME = msTIME(begin, end);

    begin = NOW();
    for (int i = 0; i < PUSH_SIZE; ++i)
        leoVec.push_back(dist(tw));

    end = NOW();
    auto leoTIME = msTIME(begin, end);
    RESULTS(stdTIME, leoTIME);
}

void test_emplace_speed()
{
    std::vector<int> stdVec;
    leo::Vector<int> leoVec;
    std::random_device rd;
    std::mt19937 tw(rd());
    std::uniform_int_distribution<int> dist(INT32_MIN, INT32_MAX);

    stdVec.reserve(PUSH_SIZE);
    leoVec.reserve(PUSH_SIZE);

    auto begin = NOW();
    for (int i = 0; i < PUSH_SIZE; ++i)
        stdVec.emplace_back(dist(tw));

    auto end = NOW();
    auto stdTIME = msTIME(begin, end);

    begin = NOW();
    for (int i = 0; i < PUSH_SIZE; ++i)
        leoVec.emplace_back(dist(tw));

    end = NOW();
    auto leoTIME = msTIME(begin, end);
    RESULTS(stdTIME, leoTIME);
}